import { combineReducers } from 'redux';

import { moviesReducer } from './movies-reducer';
import  headerReducer  from './header-reducer';

const rootReducer = combineReducers({
  movies: moviesReducer, 
  header: headerReducer
});

export default rootReducer;
