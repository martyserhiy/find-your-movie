import React from "react";
import "./styles/styles.scss"
import Footer from "./components/footer";
import Logo from "./components/logo";
import NotFound from "./components/erros/404";
import HeaderContainer from "./components/header/HeaderContainer";
import MovieListContainer from "./components/movieList/MovieListContainer";
import ErrorBoundary from "./components/ErrorBoundary";
import MovieDetailsContainer from "./components/movieDetails/MovieDetailsContainer";
import { hot } from 'react-hot-loader';
import { Provider } from "react-redux";
import {
    HashRouter as Router,
    Switch,
    Route,
} from "react-router-dom";

const App = ({
    Router, location, context, store,
}) => (
        <Provider store={store}>
            <Router>
                <HeaderContainer logo={<Logo />} />
                <Switch>
                    <Route exact path="/">
                        <ErrorBoundary>
                            <MovieListContainer />
                        </ErrorBoundary>
                    </Route>
                    <Route path="/film/:id" component={MovieListContainer}>
                        <MovieDetailsContainer logo={<Logo />} />
                        <ErrorBoundary>
                            <MovieListContainer />
                        </ErrorBoundary>
                    </Route>
                    <Route path="/search/:search">
                        <ErrorBoundary>
                            <MovieListContainer />
                        </ErrorBoundary>
                    </Route>
                    <Route path="*" component={NotFound} />
                </Switch>
                <Footer>
                    <Logo />
                </Footer>
            </Router>
        </Provider >
    );


export default hot(module)(App);