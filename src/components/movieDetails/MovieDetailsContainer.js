import MovieDetails from "."
import {connect} from "react-redux";
import {updateSort, updateFilter, updateMovieActionCreator, addMovieActionCreator, deleteMovieActionCreator, updateMovieIdActionCreator} from "../../modules/movies-reducer"

const mapStateToProps = (state) => {
    return {
        movies:state.movies.movieList,
    }
}

export default connect(mapStateToProps)(MovieDetails)