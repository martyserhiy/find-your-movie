import React from "react"

export default (props) => {
    const values = { 'year': 'release date', 'genre': 'genre', 'title': 'title' }
    return (
        <div className="filterWrapper">
            <span>sort by</span>
            <select onChange={props.onChangeSort}>
                {props.optionYear(values.year, 'year')}
                {props.optionGenre(values.genre, values.genre)}
                {props.optionTitle(values.title, values.title)}
            </select>
        </div>
    )
}