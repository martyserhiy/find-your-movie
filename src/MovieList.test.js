import { updateFilter, updateSort } from './redux/movies-reducer'
describe('>>>A C T I O N --- Test filter and sort', ()=>{
    it('+++ actionCreator updateFilter', () => {
        const filter = updateFilter('documentary')
        expect(filter).toEqual({ type:"SET-FILTER", filter:'documentary' })
    });
    it('+++ actionCreator updateSort', () => {
        const sort = updateSort('genre')
        expect(sort).toEqual({ type:"SET-SORT", sort:'genre' })
    });
});