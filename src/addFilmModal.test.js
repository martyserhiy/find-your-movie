import React from 'react';
import { render, screen } from '@testing-library/react';
import AddFilmModal from './components/modals/AddFilmModal';


describe('add new film', () => {
    it('should ', () => {
        render(<AddFilmModal/>);

        expect(screen.getByLabelText('Title'));
        expect(screen.getByLabelText('release date'));
        expect(screen.getByPlaceholderText('Runtime here'));
    })
    
});
