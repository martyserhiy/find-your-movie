module.exports = process.env.NODE_ENV === 'development'
    ? require('./conf/webpack.config.dev.js')
    : require('./conf/webpack.config.prod.js');