const { merge } = require('webpack-merge');
const path = require('path');
const common = require('./webpack.config.common');
const webpack = require('webpack');
const ASSET_PATH = process.env.PUBLIC_URL || '/';

module.exports = merge(common, {
    mode: 'production',
    output: {
        filename: '[name].bundle.js',
        chunkFilename: '[name].bundle.js',
        path: path.resolve('build', 'prod'),
        publicPath: ASSET_PATH
    },
    devServer: {
        contentBase: path.join(__dirname, 'prod'),
        compress: true,
        port: 8000,
        open: true
    },
    plugins: [
        new webpack.DefinePlugin({
            'process.env.ASSET_PATH': JSON.stringify(ASSET_PATH),
            "process.env": {
                NODE_ENV: JSON.stringify("production")
            }
        })
    ],
});